﻿namespace AppCore.Models
{
    public class BusinessInfo
    {
        
        public string PhoneNumber { get; set; }
        public string FormattedPhoneNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(PhoneNumber))
                {
                    return PhoneNumber.Replace("+", "00").Replace("-", "").Replace(" ", "");
                }
                return PhoneNumber;
            }
        }
        
        public string ContactUsEmail { get; set; }
        public string BookingEmail { get; set; }
        public string Address { get; set; }
        public string WorkingHours { get; set; }
        public string BusinessName { get; set; }
        public string WebsiteLink { get; set; }
        public string WebsiteText { get; set; }
        public string LogoFullPath { get; set; }
        public string FacebookLink { get; set; }
        public string TripAdvisorLink { get; set; }
        public string YouTubeLink { get; set; }
        public string ViberLink { get; set; }
        public string WhatsAppLink { get; set; }
        public string AdminEmail { get; set; }
        public string FaxNo { get; set; }
        public string Video { get; set; }

    }
}