﻿using AppCore.Models;
using AppCore.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppCore.Managers
{

    public class NewsLetterSubscriptionManager
    {
        private NewsletterSubscription subscriptionEntity = new NewsletterSubscription();

        AppCoreRepository<NewsletterSubscription> SubscriptionRepo;
        public NewsLetterSubscriptionManager(AppCoreContext Context, int LanguageId)
        {
            SubscriptionRepo = new AppCoreRepository<NewsletterSubscription>(Context, LanguageId);
        }

        public int FindEmail(string Email)
        {
            var result=SubscriptionRepo.GetOne(x => x.Email == Email);
            if (result == null)
            {
                return 0;
            }
           
                return 1;
           
           

        }
        public void AddSubscription(string Email)
        {

            subscriptionEntity.Email = Email;
            subscriptionEntity.SubscriptionDate = DateTime.Now;

            SubscriptionRepo.Add(subscriptionEntity);


        }
    }
}
