﻿using AppCore.Models;
using AppCore.Repositories;
using System.Linq;


namespace AppCore.Managers
{
    public class AlbumManager
    {
        private readonly AppCoreRepository<Album> _repo;
        private readonly AppCoreRepository<AlbumPhoto> _photoRepo;

        public AlbumManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<Album>(context, languageId);
            _photoRepo = new AppCoreRepository<AlbumPhoto>(context, null);
        }

        public IQueryable<Album> GetAlbums()
        {
            return _repo.GetMany(null, x => x.AlbumPhotos).OrderBy(x => x.DisplayOrder);
        }




    }
}