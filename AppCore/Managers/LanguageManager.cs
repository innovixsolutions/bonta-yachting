﻿using AppCore.Models;
using AppCore.Repositories;
using System.Linq;


namespace AppCore.Managers
{
    public class LanguageManager
    {
        private readonly AppCoreRepository<Language> _repo;

        public LanguageManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<Language>(context, languageId);
        }

        public IQueryable<Language> GetAciveLanguages()
        {
            return _repo.GetMany(x => x.Active).OrderBy(x => x.DisplayOrder);
        }
    }
}