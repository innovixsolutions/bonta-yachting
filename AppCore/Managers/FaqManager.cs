﻿using AppCore.Models;
using AppCore.Repositories;
using System.Linq;

namespace AppCore.Managers
{
    public class FaqManager
    {
        private readonly AppCoreRepository<Faq> _faqRepository;
        private readonly AppCoreRepository<FaqCategory> _faqsCategoryRepository;

        public FaqManager(AppCoreContext context, int languageId)
        {
            _faqRepository = new AppCoreRepository<Faq>(context, languageId);
            _faqsCategoryRepository = new AppCoreRepository<FaqCategory>(context, languageId);
        }

        public IQueryable<FaqCategory> GetCategories()
        {
            return _faqsCategoryRepository.GetMany(null, f => f.Faqs);
        }


        public IQueryable<Faq> GetAllFaqs()
        {
            return _faqRepository.GetMany(f => f.Active.Value, f => f.Category).OrderBy(x => x.DisplayOrder);
        }
    }
}