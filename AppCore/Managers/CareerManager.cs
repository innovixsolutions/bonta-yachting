﻿using AppCore.Models;
using AppCore.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace AppCore.Managers
{
    public class CareerManager
    {

        private readonly AppCoreRepository<Career> _careerRepo;
        public CareerManager(AppCoreContext contex, int languageId)
        {
            _careerRepo = new AppCoreRepository<Career>(contex, languageId);
        }

        public IEnumerable<Career> GetAllJobs()
        {
            return _careerRepo.GetMany(x=> x.Active, x=> x.City).OrderBy(x=> x.DisplayOrder);
        }
       
    }
}
