﻿using AppCore.Models;
using AppCore.Repositories;

namespace AppCore.Managers
{

    public class MetaTagManager
    {
        private readonly AppCoreRepository<MetaTag> _repo;

        public MetaTagManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<MetaTag>(context, languageId);
        }


        public MetaTag GetPageTags(int PageId)
        {
            

            return _repo.GetOne(m => m.PageId == PageId);

        }

    }
}
