﻿using AppCore.Models;
using System.Linq;

namespace AppCore.Managers
{
    public class MediaItemManager
    {
        private readonly AppCoreContext context;

        public MediaItemManager(AppCoreContext context)
        {

            this.context = context;
        }

        //public IQueryable<MediaItem> GetMediaItems()
        //{
        //    return context.MediaItems.Include(x => x.Collection);
        //}


        //public IQueryable<MediaItem> GetMediaItems(PageNames mediaItemPageEnum, MediaItemCollections mediaItemCollection)
        //{
        //    return context.MediaItems.Include(x => x.Collection).Include(x=>x.Collection.Page)
        //        .Where(x => x.Collection.PageId == 1
        //                               && x.CollectionId == (int)mediaItemCollection)
        //                               .OrderBy(x => x.DisplayOrder);
        //}

        //public MediaItem GetMediaItem(MediaItemKeys key, MediaItemCollections collection)
        //{
        //    var item = context.MediaItems.FirstOrDefault(x => StringUtilities.DasherizeString(x.ItemKey) == 
        //        StringUtilities.DasherizeString(key.ToString())
        //        && x.CollectionId == (int)collection);
        //    return item;
        //}

        public MediaItem GetMediaItem(string MediaItemKey, int PageId)
        {
            var item = context.MediaItems.FirstOrDefault(x => (x.ItemKey) ==
                MediaItemKey
                && x.PageId == PageId);
            return item;
        }
    }
}