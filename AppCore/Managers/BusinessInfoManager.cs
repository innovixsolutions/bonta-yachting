﻿using AppCore.Enums;
using AppCore.Models;
using AppCore.Repositories;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AppCore.Managers
{
    public class BusinessInfoManager
    {
        private readonly AppCoreRepository<Setting> _repo;
        private readonly AppCoreRepository<SocialLink> _repoSocial;
        private readonly IMemoryCache memoryCache;
        private const string BusinessInfoKey = "BusinessInfo_Cache";
        private const string SocialLinkKey = "SocialLink_Cache";
       
        public BusinessInfoManager(AppCoreContext context,IMemoryCache memoryCache)
        {

            _repo = new AppCoreRepository<Setting>(context, null);
            _repoSocial = new AppCoreRepository<SocialLink>(context, null);
            this.memoryCache = memoryCache;
        }

        public string GetSetting(string key)
        {
            return _repo.GetOne(x => x.Key == key)?.Value;
        }

        public string GetSetting(SettingType keyType)
        {
            return _repo.GetOne(s => s.Key == keyType.ToString())?.Value;
        }
        public void AddBusinessInfoToCache(BusinessInfo BusinessInfo)
        {
            memoryCache.Set<BusinessInfo>(BusinessInfoKey, BusinessInfo, TimeSpan.FromHours(1));
        }
        public BusinessInfo GetCashedBusinessInfo()
        {
            BusinessInfo BusinessInfo;
            if(!memoryCache.TryGetValue(BusinessInfoKey, out BusinessInfo))
            {
                BusinessInfo = new BusinessInfo
                {
                    Address = GetSetting(SettingType.Address),
                    ContactUsEmail = GetSetting(SettingType.ContactUsEmail),
                    PhoneNumber = GetSetting(SettingType.Telephone),
                    Video= GetSetting(SettingType.HomePageVideo)
                    
                };
                AddBusinessInfoToCache(BusinessInfo);
            }
            return BusinessInfo;
        }
        public BusinessInfo GetBusinessInfo()
        {
            return GetCashedBusinessInfo();
        }

        public string GetSocialLink(SocialLinkTypes type)
        {
            dynamic Link = GetSocialLinks().FirstOrDefault(x => x.Type == (int)type);
            return Link != null ? Link.Link : "";
        }
        public void AddSocialLinksToCache(List<SocialLink> SocialLinks)
        {
            memoryCache.Set<List<SocialLink>>(SocialLinkKey, SocialLinks, TimeSpan.FromHours(1));
        }
        public List<SocialLink> GetCachedSocialLinks()
        {
            List<SocialLink> SocialLinks;
            if (!memoryCache.TryGetValue(SocialLinkKey, out SocialLinks))
            {
                SocialLinks = _repoSocial.GetMany(x => x.Active.Value).ToList();

                AddSocialLinksToCache(SocialLinks);
            }
            return SocialLinks;
        }
        public List<SocialLink> GetSocialLinks()
        {
            return GetCachedSocialLinks();
        }
        public SocialMediaInfo GetSocialMediaInfo()
        {
            SocialMediaInfo info = new SocialMediaInfo
            {
                FacebookLink = GetSocialLink(SocialLinkTypes.Facebook),
                TwitterLink = GetSocialLink(SocialLinkTypes.Twitter),
                InstagramLink = GetSocialLink(SocialLinkTypes.Instagram),
                YouTubeLink = GetSocialLink(SocialLinkTypes.YouTube),
                LinkedInLink = GetSocialLink(SocialLinkTypes.LinkedIn),
                GooglePlusLink = GetSocialLink(SocialLinkTypes.GooglePlus),
                PinterestLink = GetSocialLink(SocialLinkTypes.Pinterest),
                SkypeLink = GetSocialLink(SocialLinkTypes.Skype),
                WhatsappLink = GetSocialLink(SocialLinkTypes.WhatsApp)
            };
            return info;
        }
        public string GetCommissionPrice()
        {
          return  GetSetting(SettingType.CommissionPrice);
        }
       
    }
}