﻿using AppCore.Models;
using AppCore.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppCore.Managers
{
    public class TestimonialManager
    {
        private readonly AppCoreRepository<Testimonial> _repo;
        public TestimonialManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<Testimonial>(context, languageId);

        }

        //public IQueryable<Testimonial> GetTestimonials()
        //{
        //   return  _repo.GetMany(null,x=>x.TestimonialSource);
        //}
    }
}
