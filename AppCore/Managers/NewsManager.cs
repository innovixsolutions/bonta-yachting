﻿using AppCore.Domain;
using AppCore.DTO;
using AppCore.Models;
using AppCore.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace AppCore.Managers
{
    public class NewsManager
    {
        private readonly AppCoreRepository<News> repo;


        public NewsManager(AppCoreContext context, int languageId)
        {
            repo = new AppCoreRepository<News>(context, languageId);
        }
        public News GetNewsDetails(string UrlName)
        {
            return repo.GetOne(x => x.UrlName == UrlName);
        }
        public IQueryable<News> GetNews()
        {
            return repo.GetMany(x => x.Active.Value).OrderByDescending(x => x.NewsDate);
        }

        public IQueryable<News> GetNews(NewsFilter newsFilter)
        {
            return repo.GetMany(x => x.Active.Value && 
            (newsFilter.Year.HasValue == false
            || x.NewsDate.Year == newsFilter.Year) &&
                        (newsFilter.Month.HasValue == false || x.NewsDate.Month == newsFilter.Month))
                .OrderByDescending(x => x.NewsDate);
        }
        //public IQueryable<News> GetNews(int Year, int Month)
        //{
        //    return repo.GetMany(x => x.Active.Value &&
        //    x.NewsDate.Year == Year && x.NewsDate.Month == Month).OrderByDescending(x => x.NewsDate);
        //}

        public IQueryable<News> GetNews(string month, int? year)
        {

            if (string.IsNullOrWhiteSpace(month) && year == null)
            {
                return GetNews();
            }

            var monthNumber = DateTime.ParseExact(month, "MMMM", CultureInfo.CurrentCulture).Month;
            return repo.GetMany(x => x.Active.Value &&
            x.NewsDate.Month == monthNumber && x.NewsDate.Year == year.Value).OrderByDescending(x => x.NewsDate);
        }

        public IQueryable<News> GetRecentNews(int currentNewsId)
        {
            return repo.GetMany(x => x.Id != currentNewsId
            && x.Active.Value).OrderByDescending(x => x.NewsDate);
        }



        public News GetNextNews(int newsId)
        {
            return repo.GetAll().LastOrDefault(x => x.Id > newsId);

        }

        public News GetPreviousNews(int newsId)
        {
            return repo.GetAll().LastOrDefault(x => x.Id < newsId);
        }

        public IEnumerable<ArchiveModel> GetNewsArchive(IEnumerable<News> news)
        {
            if (news != null && news.Any())
            {
                return news.GroupBy(x => x.NewsDate.Year).Select(c => new ArchiveModel
                {
                    Year = c.Key,
                    Months = c.GroupBy(x => x.NewsDate.Month).Select(m => new GroupedMonth { Month = m.Key, NewsList = m, Year = c.Key })
                });
            }

            return default;
        }


    }
}




