﻿using AppCore.Models;
using AppCore.Repositories;
using System.Collections.Generic;
using System.Linq;


namespace AppCore.Managers
{
    public class HomeBannerManager
    {
        private readonly AppCoreRepository<HomeBanner> _repo;

        public HomeBannerManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<HomeBanner>(context, languageId);
        }

        public IQueryable<HomeBanner> GetHomeBanners()
        {
            return _repo.GetMany(x => x.Active.Value).OrderBy(x => x.DisplayOrder);
        }
    }
}