﻿using AppCore.Models;
using AppCore.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace AppCore.Managers
{
    public class InfoPageManager
    {
        private readonly AppCoreRepository<InfoPage> _repo;

        public InfoPageManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<InfoPage>(context, languageId);
        }

        public List<InfoPage> GetInfoPages()
        {
            return _repo.GetMany(x => x.Active.Value)
                .OrderBy(x => x.DisplayOrder).ThenByDescending(x => x.Id).ToList();
        }
      
        public InfoPage GetInfoPage(string urlName)
        {
            return _repo.GetOne(x => x.UrlName.ToLower() == urlName.ToLower());
        }
    }
}
