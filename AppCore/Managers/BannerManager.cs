﻿using AppCore.Domain;
using AppCore.Enums;
using AppCore.Models;
using AppCore.Repositories;
using System.Collections.Generic;
using System.Linq;


namespace AppCore.Managers
{
    public class BannerManager
    {
        private readonly AppCoreRepository<Banner> _repo;

        public BannerManager(AppCoreContext context, int languageId)
        {
            _repo = new AppCoreRepository<Banner>(context, languageId);
        }



        public Banner GetBanner(int PageId)
        {
            return _repo.GetOne(x => x.PageId == PageId && x.Active.Value);
        }

        public IEnumerable<Banner> GetBanners()
        {
            return _repo.GetMany(x => x.Active.Value).OrderBy(x => x.DisplayOrder);
        }



    }
}