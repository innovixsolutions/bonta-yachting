﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace AppCore.Utilities
{
    public static class CollectionUtilities
    {

        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> collection)
        {
           return collection.Select((item, index) => (item, index));
        }

        public static void Put<T>(this ITempDataDictionary tempData, string key, T value) where T : class
        {
            tempData[key] = JsonConvert.SerializeObject(value);
        }

        public static T Get<T>(this ITempDataDictionary tempData, string key) where T : class
        {
            object o;
            tempData.TryGetValue(key, out o);
            return o == null ? null : JsonConvert.DeserializeObject<T>((string)o);
        }

        public static void Put<T>(this ISession session, string key, T value) where T : class
        {
            var serializedObject = JsonConvert.SerializeObject(value);
            session.SetString(key, serializedObject);
        }

        public static T Get<T>(this ISession session, string key) where T : class
        {
            var deSerializedObject = session.GetString(key);
            return JsonConvert.DeserializeObject<T>(deSerializedObject);
        }
    }

   
}
