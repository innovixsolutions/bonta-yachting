﻿using AppCore.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AppCore.Repositories
{
    public abstract class BaseRepository<TEntity>  where TEntity : class
    {
        protected readonly DbContext Context;
        protected DbSet<TEntity> DbSet;
        protected int? LanguageId;

        public string connectionString { get; set; }
        
        public BaseRepository(DbContext context, int? languageId = null)
        {
            LanguageId = languageId;
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public TEntity Get<TKey>(TKey id)
        {
            return DbSet.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> entities = DbSet;
            entities = Localize(entities);
            return entities;
        }

        public virtual TEntity GetOne(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return GetMany(where, includeProperties).FirstOrDefault();
        }

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = where == null
                ? DbSet
                : DbSet.Where(where);
            var entities = includeProperties.Aggregate(query, (current, includeProperty) =>
                current.Include(includeProperty));
            entities = Localize(entities, includeProperties);

            return entities;
        }
        

        #region Localization Methods
        private IQueryable<TEntity> Localize(IQueryable<TEntity> entities, Expression<Func<TEntity, object>>[] includeProperties = null)
        {
            //Make sure to include the localized entity
            entities = IncludeLocalizedEntities<TEntity>(entities, typeof(TEntity));
            //Include all the localized entities for the includes
            if (includeProperties != null)
                entities = includeProperties.Aggregate(entities,
                    (current, expression) => IncludeLocalizedEntities(current, GetObjectType(expression), expression));

            foreach (TEntity entity in entities.AsEnumerable())
            {
                Localize(entity, includeProperties);
            }

            return entities;
        }

        private void Localize(TEntity entity, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            if (IsLocalized(typeof(TEntity)))
                ((ILocalizedEntity)entity).Localize(LanguageId.Value);

            if (includeProperties != null)
                foreach (var expression in includeProperties)
                {
                    var includeType = GetObjectType<TEntity>(expression);
                    var nonGenericType = GetObjectGenericType<TEntity>(expression);

                    var navigationProperty = GetPropertyNavigationString(expression);

                    var localizedEntity = GetProp(typeof(TEntity), navigationProperty);

                    if (IsLocalized(nonGenericType) && localizedEntity != null)
                    {
                        if (includeType.IsGenericType)
                        {
                            var childEntities = GetPropValue<TEntity>(entity, navigationProperty) as IEnumerable;
                            if (childEntities != null)
                                foreach (var childEntity in childEntities)
                                {
                                    if (childEntity != null)
                                        ((ILocalizedEntity)childEntity).Localize(LanguageId.Value);
                                }
                        }
                        else
                        {
                            var property = (ILocalizedEntity)GetPropValue<TEntity>(entity, navigationProperty);
                            if (property != null)
                                property.Localize(LanguageId.Value);
                        }

                    }
                }
        }

        private IQueryable<TEntity> IncludeLocalizedEntities<T>(IQueryable<TEntity> query, Type type, Expression<Func<T, object>> expr = null)
        {
            if (type.IsGenericType)
            {
                type = type.GetGenericArguments()[0];
            }
            //Check for localization
            if (IsLocalized(type))
            {
                IEnumerable<PropertyInfo> localizedTypes = type.GetProperties()
                       .Where(x => x.Name.Contains("Localized"));

                if (localizedTypes.Any())
                {
                    if (type == typeof(TEntity))
                        return localizedTypes.Aggregate(query, (current, localizedType) =>
                            current.Include(localizedType.Name));
                    else
                    {
                        var nvaigationString = GetPropertyNavigationString(expr);

                        return localizedTypes.Aggregate(query, (current, localizedType) =>
                            current.Include(nvaigationString + "." + localizedType.Name));
                    }
                }
            }
            return query;
        }
        #endregion

        private string GetPropertyNavigationString(LambdaExpression expr, string breadCrumb = null)
        {
            string nvaigationString = null;
            if ((expr.Body as MemberExpression) != null)
            {
                var memberNames = new Stack<string>();

                MemberExpression memberExp = (MemberExpression)expr.Body;
                memberNames.Push(memberExp.Member.Name);
                while (memberExp.Expression is MemberExpression && TryFindMemberExpression(memberExp.Expression, out memberExp))
                {
                    memberNames.Push(memberExp.Member.Name);
                }

                nvaigationString = string.Join(".", memberNames.ToArray());
            }

            else if ((expr.Body as MethodCallExpression) != null)
            {
                var experssionArguments = ((MethodCallExpression)expr.Body).Arguments;
                breadCrumb += ((MemberExpression)experssionArguments[0]).Member.Name + ".";

                breadCrumb = experssionArguments.Skip(1)
                    .Aggregate(breadCrumb,
                        (current, argument) =>
                            current + (GetPropertyNavigationString(argument as LambdaExpression, current) + "."));
                nvaigationString = breadCrumb.TrimEnd('.');
            }
            return nvaigationString.TrimEnd('.');
        }

        private bool TryFindMemberExpression(Expression expr, out MemberExpression memberExp)
        {
            memberExp = expr as MemberExpression;
            if (memberExp != null)
            {
                return true;
            }
            if ((expr.NodeType == ExpressionType.Convert) ||
                (expr.NodeType == ExpressionType.ConvertChecked) && expr is UnaryExpression)
            {
                memberExp = ((UnaryExpression)expr).Operand as MemberExpression;
                if (memberExp != null)
                {
                    return true;
                }
            }

            return false;
        }

        public PropertyInfo GetProp(Type baseType, string propertyName)
        {
            string[] parts = propertyName.Split('.');

            if (baseType.IsGenericType)
                baseType = baseType.GenericTypeArguments[0];

            if (parts.Length > 1)
            {
                return GetProp(baseType.GetProperty(parts[0]).PropertyType,
                    parts.Skip(1).Aggregate((a, i) => a + "." + i));
            }

            return baseType.GetProperty(propertyName);
        }

        public static object GetPropValue<T>(T obj, string propName)
        {
            string[] nameParts = propName.Split('.');
            var type = obj.GetType();

            if (nameParts.Length == 1)
            {
                if (type.IsGenericType)
                {
                    return (from object childEntity in (IEnumerable)obj select type.GenericTypeArguments[0].GetProperty(propName).GetValue(childEntity, null)).ToList();
                }
                return type.GetProperty(propName).GetValue(obj, null);
            }

            foreach (string part in nameParts)
            {
                if (type.IsGenericType)
                {
                    var values = new List<object>();
                    foreach (var childEntity in (IEnumerable)obj)
                    {
                        PropertyInfo info = type.GenericTypeArguments[0].GetProperty(part);
                        if (info == null)
                        { return null; }

                        values.Add(GetPropValue(info.GetValue(childEntity, null),
                            string.Join(".", nameParts.Skip(1))));
                    }
                    return values;
                }
                else
                {
                    PropertyInfo info = type.GetProperty(part);
                    if (info == null)
                    { return null; }

                    return GetPropValue(info.GetValue(obj, null),
                            string.Join(".", nameParts.Skip(1)));
                }

            }
            return obj;
        }

        private Type GetObjectType<T>(Expression<Func<T, object>> expr)
        {
            if ((expr.Body.NodeType == ExpressionType.Convert) ||
                (expr.Body.NodeType == ExpressionType.ConvertChecked))
            {
                var unary = expr.Body as UnaryExpression;
                if (unary != null)
                    return unary.Operand.Type;
            }
            return expr.Body.Type;
        }

        private Type GetObjectGenericType<T>(Expression<Func<T, object>> expr)
        {
            Type type = GetObjectType<T>(expr);
            if (type.IsGenericType)
                type = type.GetGenericArguments().FirstOrDefault();
            return type;
        }

        private bool IsLocalized(Type type)
        {
            return typeof(ILocalizedEntity).IsAssignableFrom(type) && LanguageId.HasValue;
        }

        public void Update(TEntity entity)
        {
            Save();
        }
        private void Save()
        {
            Context.SaveChanges();
        }
        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);

            Save();
        }

    }
}
