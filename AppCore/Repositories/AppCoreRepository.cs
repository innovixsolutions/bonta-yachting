﻿using Microsoft.EntityFrameworkCore;

namespace AppCore.Repositories
{
    public class AppCoreRepository<TEntity> : BaseRepository<TEntity> where TEntity : class
    {
        public AppCoreRepository(DbContext context, int? languageId) : base(context, languageId)
        {

        }
    }
}
