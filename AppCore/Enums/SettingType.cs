﻿namespace AppCore.Enums
{
    public enum SettingType : int
    {
        NewsletterListId = 1,
        MailChimpApiKey,
        ContactUsEmail,
        AdminEmail,
        WokringHours,
        Address,
        Telephone,
        BusinessName,
        Email, Intro, DomainName, WebsiteLink,
        WeatherApiKey,
        HomePageVideo,
        CommissionPrice
    }
}