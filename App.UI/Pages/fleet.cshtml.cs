using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.UI.Enums;
using App.UI.Models;
using BontaServices.DTO;
using BontaServices.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace App.UI.Pages
{
    public class fleetModel : PageModelBase
    {
        private readonly BoatManager boatManager;

        public IQueryable<BoatDTO> Boats { get; set; }
        public fleetModel(BoatManager  boatManager)
        {
            PageName = PageNames.Fleet;
            this.boatManager = boatManager;
        }
        public void OnGet()
        {
            Boats = boatManager.GetBoats();
        }
    }
}
