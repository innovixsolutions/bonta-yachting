using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.UI.Enums;
using App.UI.Models;
using AppCore.Managers;
using AppCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace App.UI.Pages
{
    public class galleryModel : PageModelBase
    {
        private readonly AlbumManager albumManager;

        public IQueryable<Album> Albums { get; set; }
        public galleryModel(AlbumManager albumManager)
        {
            PageName=PageNames.Gallery;
            this.albumManager = albumManager;
        }
        public void OnGet()
        {
            Albums = albumManager.GetAlbums();
        }
    }
}
