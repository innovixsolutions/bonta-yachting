using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.UI.Enums;
using App.UI.Models;
using BontaServices.DTO;
using BontaServices.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace App.UI.Pages
{
    public class attractionsModel : PageModelBase
    {
        public IQueryable<AttractionDTO> Attractions { get; set; }
        public AttractionManager AttractionManager { get; }

        public attractionsModel(AttractionManager attractionManager)
        {
            PageName = PageNames.Attractions;
            AttractionManager = attractionManager;
        }
        public void OnGet()
        {
            Attractions=AttractionManager.GetAttractions();
        }
    }
}
