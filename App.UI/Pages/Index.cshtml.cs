﻿using App.UI.Models;
using App.UI.Enums;
using Microsoft.AspNetCore.Mvc;
using BontaServices.Managers;
using System.Linq;
using BontaServices.Models;
using BontaServices.DTO;
using AppCore.Models;
using AppCore.Managers;

namespace App.UI.Pages
{
    public class IndexModel : PageModelBase
    {
        private readonly BoatManager boatManager;
        private readonly HomeBannerManager homeBannerManager;

        public IQueryable<BoatDTO> Boats { get; set; } 
        public IQueryable<HomeBanner> HomeBanners { get; set; }
        public IndexModel(BoatManager boatManager,HomeBannerManager homeBannerManager )
        {
            PageName = PageNames.Home;
            this.boatManager = boatManager;
            this.homeBannerManager = homeBannerManager;
        }


        public IActionResult OnGet()
        {
            Boats = boatManager.GetBoats();
            HomeBanners=homeBannerManager.GetHomeBanners();
            return Page();
        }

       
           
        }
    }

