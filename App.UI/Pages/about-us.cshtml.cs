﻿using App.UI.Enums;
using App.UI.Models;
using AppCore.Managers;
using AppCore.Models;
using BontaServices.Managers;
using BontaServices.Models;
using System.Linq;

namespace App.UI.Pages
{
    public class about_usModel : PageModelBase
    {
        private readonly TeamManager teamManager;
        private readonly MediaItemManager mediaItemManager;

        public IQueryable<TeamMember> Members { get; set; }
        public MediaItem RightImage { get; set; }
        public about_usModel(TeamManager teamManager,MediaItemManager mediaItemManager )
        {
            PageName = PageNames.AboutUs;
            this.teamManager = teamManager;
            this.mediaItemManager = mediaItemManager;
        }
        public void OnGet()
        {
            Members=teamManager.GetTeamMembers();
            RightImage = mediaItemManager.GetMediaItem(MediaItemKeys.RightImage.ToString(),(int) PageNames.AboutUs);
        }
    }
}