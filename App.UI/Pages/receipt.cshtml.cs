using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.UI.Models;
using AppCore.Managers;
using BontaServices.Managers;
using BontaServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace App.UI.Pages
{
    public class receiptModel : PageModelBase
    {
        private readonly BookingManager bookingManager;
        private readonly BusinessInfoManager businessInfoManager;

        public BoatBooking CurrentBooking { get; set; }
        public int CommissionPrice { get; set; }
        public receiptModel(BookingManager bookingManager,BusinessInfoManager  businessInfoManager)
        {
            this.bookingManager = bookingManager;
            this.businessInfoManager = businessInfoManager;
        }
        public IActionResult OnGet(string id)
        {

            CurrentBooking = bookingManager.GetBoatBooking(id);
            CommissionPrice= Convert.ToInt32(businessInfoManager.GetCommissionPrice());
            if (CurrentBooking == null)
            {
                Redirect("/error");
            }
            if (CurrentBooking.PaymentStatus == 3)
            {
                Redirect("/error?msg=payment failed");
            }
            return Page();
        }
    }
}
