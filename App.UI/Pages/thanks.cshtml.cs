using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.UI.Models;
using AppCore.Models;
using BontaServices.Managers;
using BontaServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using X.Paymob.CashIn;

namespace App.UI.Pages
{
    public class thanksModel : PageModelBase
    {
        private readonly BookingManager bookingManager;

      
        
        public BoatBooking CurrentBooking { get; set; }
    

        public thanksModel(BookingManager bookingManager)
        {
        
            this.bookingManager = bookingManager;
        }
        public IActionResult OnGet(string id)
        {

            CurrentBooking = bookingManager.GetBoatBooking(id);
            if (CurrentBooking == null)
            {
                Redirect("/error");
            }
            if (CurrentBooking.PaymentStatus == 3)
            {
                Redirect("/error?msg=payment failed");
            }
            return Page();
        }
    }
}