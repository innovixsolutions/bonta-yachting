using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using App.UI.InfraStructure;
using App.UI.Models;
using AppCore.DTO;
using AppCore.Managers;
using BontaServices.DTO;
using BontaServices.Managers;
using BontaServices.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using X.Paymob.CashIn;
using X.Paymob.CashIn.Models;
using X.Paymob.CashIn.Models.Payment;

namespace App.UI.Pages
{

    public class boat_detailsModel : PageModelBase
    {
        private readonly BoatManager boatManager;
        private readonly ResourceInfo resourceInfo;
        private readonly MailManager mailManager;
        private readonly BookingManager bookingmanager;
        private readonly IPaymobCashInBroker broker;
        private readonly CashInConfig cashInConfig = new CashInConfig();
        private readonly IOptions<PaymobConfiguration> paymobOptions;
        private readonly BusinessInfoManager businessInfo;
        private readonly AgentCodeManager agentCodeManager;

        [BindProperty(SupportsGet = true)]
        public string UrlName { get; set; }
        [BindProperty]
        public BoatBookingDTO BoatBooking { get; set; }

        [BindProperty]
        public string AgentCode { get; set; }
        public BoatDTO Boat { get; set; }
        public IQueryable<BoatDTO> Boats { get; set; }
        [Required]
        [GoogleReCaptchaValidation]
        [BindProperty(Name = "g-recaptcha-response")]
        public string GoogleReCaptchaResponse { get; set; }
        public string PaymentIFrameSrc { get; set; }

        public List<string> DateBlock { get; set; }
        public List<string> BoatDestinations { get; set; }

        public string DateBlockJson { get; set; }

        public int CommissionPrice { get; set; }
        public boat_detailsModel(BoatManager boatManager, ResourceInfo resourceInfo,
            MailManager mailManager, BookingManager bookingmanager, IPaymobCashInBroker broker,
            IOptions<PaymobConfiguration> paymobOptions, BusinessInfoManager businessInfo,AgentCodeManager agentCodeManager)
        {
            this.boatManager = boatManager;
            this.resourceInfo = resourceInfo;
            this.mailManager = mailManager;
            this.bookingmanager = bookingmanager;
            this.broker = broker;
            this.paymobOptions = paymobOptions;
            this.businessInfo = businessInfo;
            this.agentCodeManager = agentCodeManager;
            PageName = Enums.PageNames.FleetDetails;
        }
        public void OnGet()
        {
            Boat = boatManager.GetBoat(UrlName);
            Boats = boatManager.GetOtherBoats(UrlName);
            DateBlock = boatManager.GetDateBlock(Boat.Id);
            DateBlockJson = JsonConvert.SerializeObject(DateBlock);
            BoatDestinations = boatManager.GetBoatDestinationt(Boat.Id);
            CommissionPrice = Convert.ToInt32(businessInfo.GetCommissionPrice());


            if (!string.IsNullOrEmpty(Boat.MetaTitle))
            {
                ViewData["Title"] = Boat.MetaTitle;
            }
            if (!string.IsNullOrEmpty(Boat.MetaDescription))
            {
                ViewData["MetaDescription"] = Boat.MetaDescription;
            }
        }
        public IActionResult OnPost()
        {
            if (GoogleReCaptchaResponse == null)
            {
                Notify(resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "RobottestMessage")
                , resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "ErrorTitle"),
                NotificationType.error);
                return Page();
            }

            Boat = boatManager.GetBoat(UrlName);
            Boats = boatManager.GetOtherBoats(UrlName);
            DateBlock = boatManager.GetDateBlock(Boat.Id);
            DateBlockJson = JsonConvert.SerializeObject(DateBlock);
            BoatDestinations = boatManager.GetBoatDestinationt(Boat.Id);
            BoatBooking.BoatId = Boat.Id;
            BoatBooking.BoatName = Boat.LocalizedName;
            CommissionPrice = Convert.ToInt32(businessInfo.GetCommissionPrice());
         
            if ((string.IsNullOrEmpty(AgentCode))||(!agentCodeManager.IsAgentCodeValid(AgentCode)))
            {


                var BookingResult = bookingmanager.SaveBoatBooking(BoatBooking);
                if (!BookingResult.IsSuccess)
                {
                    Notify(BookingResult.Error,
                    resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "ErrorTitle"), NotificationType.error);

                    return Page();
                }


                PaymentIFrameSrc = GetPaymentIFrameSrc(BoatBooking.BookingKey.ToString());
                return Page();

            }
            // if AgentCode is VALID
             
            BoatBooking.Price = 0;
            var BookingResultAgent = bookingmanager.SaveBoatBooking(BoatBooking);
            if (!BookingResultAgent.IsSuccess)
            {
                Notify(BookingResultAgent.Error,
                resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "ErrorTitle"), NotificationType.error);

                return Page();
            }


            bookingmanager.AddBoatBlockedDate(BoatBooking.CheckIn, BoatBooking.BoatId);

            MailInfo CustomerEmail = new MailInfo
            {
                Title = "Boat Booking Request",
                Model = BoatBooking,
                ToMail = BoatBooking.Email,
                Subject = "Boat Booking",
                TemplateName = "CustomerConfirmation",
            };
            var result = mailManager.SendRequest(CustomerEmail);

            var BontaEmail = new MailInfo
            {
                Title = "Boat Booking Confirmation",
                Model = BoatBooking,
                ToMail = BusinessInfo.ContactUsEmail,
                Subject = $"Boat Booking Request: #{BoatBooking.BookingKey} - {BoatBooking.FirstName}",
                TemplateName = "BontaConfirmation",
            };
            var result2 = mailManager.SendRequest(BontaEmail);

            if (result.IsFailure || result2.IsFailure)
            {
                Notify(result.Error,
                resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "ErrorTitle"), NotificationType.error);

                return Page();
            }
            return RedirectAndNotify(GetLocalizedUrl($"/fleet-details/{UrlName}"),
               resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "SuccessMessage")
           , resourceInfo.GetLocalizedSharedString(CurrentFullLanguageTag, "SuccessTitle"),
           NotificationType.success
           );




        }
        private string GetPaymentIFrameSrc(string merchantOrderId)
        {
            var billingInfo = new CashInBillingData(
                            firstName: BoatBooking.FirstName,
                            lastName: BoatBooking.LastName,
                            phoneNumber: BoatBooking.Phone,
                            email: BoatBooking.Email);
            var egpAmount = Convert.ToInt32(businessInfo.GetCommissionPrice());
            var intergrationId = paymobOptions.Value.TestMode ? paymobOptions.Value.TestIntegrationId : paymobOptions.Value.LiveIntegrationId;
            PaymobService paymob = new PaymobService(broker, intergrationId);

            return paymob.RequestCardPaymentKey(merchantOrderId, (int)egpAmount, billingInfo).Result;
        }
    }
}
