﻿using App.UI.Models;
using App.UI.Enums;
using Microsoft.AspNetCore.Mvc;

namespace App.UI.Pages
{
    public class ErrorModel : PageModelBase
    {
        public string ErrorMessage { get; set; }
        public ErrorModel()
        {
            PageName = PageNames.Error;
        }
        public IActionResult OnGet(string msg)
        {
            this.ErrorMessage = msg;
            
            return Page();
        }
    }
}