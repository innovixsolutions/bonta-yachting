using App.UI.Enums;
using App.UI.Models;
using AppCore.Common;
using AppCore.DTO;
using AppCore.Managers;
using AppCore.Models;
using BontaServices.DTO;
using BontaServices.Managers;
using BontaServices.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using X.Paymob.CashIn;
using X.Paymob.CashIn.Models.Callback;

namespace App.UI.Pages
{
    public class process_paymentModel : PageModelBase
    {
        private readonly IPaymobCashInBroker broker;
        private readonly MailManager mailManager;
        private readonly BookingManager bookingManager;
       
        [BindProperty(SupportsGet = true)]
        public CashInCallbackTransaction CallBackData { get; set; }

        [BindProperty(SupportsGet = true)]
        public string hmac { get; set; }
        public BoatBooking currentBooking { get; set; }
        public process_paymentModel(IPaymobCashInBroker broker, MailManager mailManager,BookingManager  bookingManager)
        {
            this.broker = broker;
            this.mailManager = mailManager;
            this.bookingManager = bookingManager;
         

        }



        public void OnGet()
        {


            var bookingId = Request.Query["merchant_order_id"];
           

           currentBooking = bookingManager.GetBoatBooking(bookingId);
            if (CallBackData.Success)
            {
                currentBooking.PaymentStatus= (int)PaymentStatus.Successful;
                 bookingManager.UpdateBoatBooking(currentBooking);
                bookingManager.AddBoatBlockedDate(currentBooking.CheckIn,currentBooking.BoatId);
                   SendNotificationEmail(currentBooking);

                   Response.Redirect($"/receipt?id={bookingId}");
                
                

            }
            else
            {
                currentBooking.PaymentStatus = (int)PaymentStatus.Failed;
                bookingManager.UpdateBoatBooking(currentBooking);
                

                Response.Redirect($"/error?msg=Payment Failed, Please try again");
            }
        }

        private Result SendNotificationEmail(BoatBooking booking)
        {
            var emailModel = new BoatBookingDTO
            {
                FirstName = booking.Customer.FirstName,
                LastName = booking.Customer.LastName,
                Email = booking.Customer.Email,
                Phone = booking.Customer.Phone,
                BookingKey = booking.BookingKey,
                CheckIn = booking.CheckIn,
                Price = booking.TotalPrice,
                Destination= booking.Destination,
                Adults=booking.Adults,
                Children=booking.Children,
                BoatName = booking.Boat.UrlName.Replace("-", " "),
                Message=booking.SpecialRequest,
                FacebookLink = BusinessInfo.FacebookLink,
                BusinessName = BusinessInfo.BusinessName,
                WebsiteLink = BusinessInfo.WebsiteLink,
                LogoFullPath = BusinessInfo.LogoFullPath,
                HostName = DomainName,
              

            };

            var BontaEmail = new MailInfo
            {
                Title = "Boat Booking Confirmation",
                Model = emailModel,
                ToMail = BusinessInfo.ContactUsEmail,
                Subject = $"Boat Booking Request: #{booking.BookingKey} - {booking.Customer.FirstName}",
                TemplateName = "BontaConfirmation",
            };
            mailManager.SendRequest(BontaEmail);

            var CustomerEmail = new MailInfo
            {
                Title = "Boat Booking Request",
                Model = emailModel,
                ToMail = booking.Customer.Email,
                Subject = "Boat Booking",
                TemplateName = "CustomerConfirmation",
            };
          return mailManager.SendRequest(CustomerEmail);

        }
    }
}
