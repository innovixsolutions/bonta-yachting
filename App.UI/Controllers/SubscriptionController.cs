﻿using AppCore.Managers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.UI.Controllers
{
    [Route("api/[controller]/[action]")]
  
    public class SubscriptionController : Controller
    {
        private readonly NewsLetterSubscriptionManager _SubscriptionManager;
        public SubscriptionController(NewsLetterSubscriptionManager subscriptionManager)
        {
            _SubscriptionManager = subscriptionManager;
        }
        [HttpPost]
        public JsonResult AddSubscription(string Email)
        {
            var result = _SubscriptionManager.FindEmail(Email);
            if (result == 0)
            {
                _SubscriptionManager.AddSubscription(Email);
                return new JsonResult(1);
            }

            return new JsonResult(0);

        }
    }
}



