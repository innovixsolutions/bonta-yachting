﻿namespace App.UI.Enums
{
    public enum PaymentStatus : int
    {
        Pending=1,
        Successful,
        Failed
    }
}
