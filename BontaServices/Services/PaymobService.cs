﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.Paymob.CashIn;
using X.Paymob.CashIn.Models;
using X.Paymob.CashIn.Models.Orders;
using X.Paymob.CashIn.Models.Payment;

namespace BontaServices.Services
{
  
        public class PaymobService
        {
            private readonly int integrationId;
            private readonly IPaymobCashInBroker _broker;

        public PaymobService(IPaymobCashInBroker broker, int integrationId)
        {
            _broker = broker;
            this.integrationId = integrationId;
        }


        public async Task<string> RequestCardPaymentKey(string merchantOrderId, decimal amount, CashInBillingData billingInfo)
            {

                int amountCents = (int)Math.Ceiling(amount * 100); // 10 LE
                var orderRequest = CashInCreateOrderRequest.CreateOrder(amountCents, "EGP", merchantOrderId);

                var orderResponse = await _broker.CreateOrderAsync(orderRequest);

                // Request card payment key.


                var paymentKeyRequest = new CashInPaymentKeyRequest(
                    integrationId: integrationId,   
                    orderId: orderResponse.Id,
                    billingData: billingInfo,
                    amountCents: amountCents);

                var paymentKeyResponse = await _broker.RequestPaymentKeyAsync(paymentKeyRequest);

                // Create iframe src.
                return $"https://accept.paymob.com/api/acceptance/iframes/395488?payment_token={paymentKeyResponse.PaymentKey}";
            }
        }
    
}
