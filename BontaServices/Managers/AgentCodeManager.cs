﻿using BontaServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.Managers
{
    public class AgentCodeManager
    {
        private readonly BontaContext bontaContext;

        public AgentCodeManager(BontaContext bontaContext)
        {
            this.bontaContext = bontaContext;
        }

        public bool IsAgentCodeValid(string AgentCode)
        {
            if (bontaContext.AgentCode.Where(x => x.Code == AgentCode).Any())
            {
                return true;
            }
            return false;
        }
    }
}
