﻿using BontaServices.DTO;
using BontaServices.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.Managers
{
    public class AttractionManager
    {
        private readonly BontaContext bontaContext;
        private readonly int languageId;

        public AttractionManager(BontaContext bontaContext,int LanguageId)
        {
            this.bontaContext = bontaContext;
            languageId = LanguageId;
        }
        public IQueryable<AttractionDTO> GetAttractions()
        {
            return bontaContext.Attraction.Where(a => a.Active.Value).Include(a => a.AttractionLocalized.
            Where(l=>l.LanguageId==languageId)).OrderBy(a=>a.DisplayOrder)
                .Select(a => new AttractionDTO
                {
                    Title = a.AttractionLocalized.Select(a=>a.Title).FirstOrDefault(),
                    SubTitle = a.AttractionLocalized.Select(a=>a.SubTitle).FirstOrDefault(),
                    Highlights = a.AttractionLocalized.Select(a=>a.Highlights).FirstOrDefault(),
                    Photo=a.Photo
                });

            
        } 
    }
}
