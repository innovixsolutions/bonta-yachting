﻿using BontaServices.DTO;
using BontaServices.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.Managers
{
    public class BoatManager
    {
        private readonly BontaContext bontaContext;
        private readonly int languageId;

        public BoatManager(BontaContext bontaContext, int LanguageId)
        {
            this.bontaContext = bontaContext;
            languageId = LanguageId;
        }


        public IQueryable<BoatDTO> GetBoats()
        {
            var boats = bontaContext.Boat.Where(b => b.Active.Value).OrderBy(b => b.DisplayOrder)

               .Include(b => b.BoatLocalized.Where(boatlocalized => boatlocalized.LanguageId == languageId));


               return Assign(boats);
        }
        public IQueryable<BoatDTO> GetOtherBoats(string UrlName)
        {
            var boats = bontaContext.Boat.Where(b => b.Active.Value&&b.UrlName!=UrlName).OrderBy(b => b.DisplayOrder)

              .Include(b => b.BoatLocalized.Where(boatlocalized => boatlocalized.LanguageId == languageId));


            return Assign(boats);
        }

        private IQueryable<BoatDTO> Assign(IIncludableQueryable<Boat, IEnumerable<BoatLocalized>>  boats)
        {


            var boatsDto = boats.Select(boat => new BoatDTO
            {
                BannerPhoto = boat.BannerPhoto,
                LocalizedName = boat.BoatLocalized.Select(b => b.Name).FirstOrDefault(),
                Description = boat.BoatLocalized.Select(b => b.Description).FirstOrDefault(),
                Draft = boat.Draft,
                Exclude = boat.BoatLocalized.Select(b => b.Exclude).FirstOrDefault(),
                Include = boat.BoatLocalized.Select(b => b.Include).FirstOrDefault(),
                HullMaterial = boat.BoatLocalized.Select(b => b.HullMaterial).FirstOrDefault(),
                Length = boat.Length,
                MainPhoto = boat.MainPhoto,
                UrlName = boat.UrlName,
                MetaDescription = boat.BoatLocalized.Select(b => b.MetaDescription).FirstOrDefault(),
                MetaTitle = boat.BoatLocalized.Select(b => b.MetaTitle).FirstOrDefault(),
                MaxSpeed = boat.MaxSpeed,
                Passengers=boat.Passengers,
                
                
            });
            return boatsDto;
        }

        public BoatDTO GetBoat(String UrlName)
        {
            var boat = bontaContext.Boat.Where(b => b.Active.Value && b.UrlName == UrlName)

             .Include(b => b.BoatLocalized.Where(boatlocalized => boatlocalized.LanguageId == languageId))
             .Include(boat => boat.BoatPhotos).Include(boat=>boat.BoatRate)
             .Select(boat => new BoatDTO
             {
                 Id=boat.Id,
                 BannerPhoto = boat.BannerPhoto,
                 LocalizedName = boat.BoatLocalized.Select(b => b.Name).FirstOrDefault(),
                 Description = boat.BoatLocalized.Select(b => b.Description).FirstOrDefault(),
                 Draft = boat.Draft,
                 Exclude = boat.BoatLocalized.Select(b => b.Exclude).FirstOrDefault(),
                 Include = boat.BoatLocalized.Select(b => b.Include).FirstOrDefault(),
                 HullMaterial = boat.BoatLocalized.Select(b => b.HullMaterial).FirstOrDefault(),
                 Length = boat.Length,
                 MainPhoto = boat.MainPhoto,
                 UrlName = boat.UrlName,
                 MetaDescription = boat.BoatLocalized.Select(b => b.MetaDescription).FirstOrDefault(),
                 MetaTitle = boat.BoatLocalized.Select(b => b.MetaTitle).FirstOrDefault(),
                 MaxSpeed = boat.MaxSpeed,
                 BoatPhotos = boat.BoatPhotos.ToList(),
                 Make = boat.BoatLocalized.Select(b => b.Make).FirstOrDefault(),
                 Type = boat.BoatLocalized.Select(b => b.Type).FirstOrDefault(),
                 Passengers = boat.Passengers,
                 Extras = boat.BoatLocalized.Select(b => b.Extras).FirstOrDefault(),
                 Notes = boat.BoatLocalized.Select(b => b.Notes).FirstOrDefault(),
                 WhatToBring= boat.BoatLocalized.Select(b => b.WhatToBring).FirstOrDefault(),
                 Price=boat.BoatRate.Where(p=>p.ValidFrom<=DateTime.Today&& DateTime.Today<=p.ValidTill).FirstOrDefault().Price,
                 
                
    }).AsSingleQuery();


            return boat.FirstOrDefault();

        }

        public List<string> GetDateBlock(int BoatId)
        {
            List<string> DateBlocks = new List<string>();
            var DateBlock = bontaContext.BoatBlockedDate.Where(b => b.BoatId == BoatId);
            foreach (var item in DateBlock)
            {
               
                    DateBlocks.Add(item.BlockDate.ToString("dd-MM-yyyy"));
               
            }
            return DateBlocks;
        }

        public List<string> GetBoatDestinationt(int BoatId)
        {
            List<string> BoatDestinationList = new List<string>();
            var BoatDestinationts = bontaContext.BoatAttriactionList.Where(b => b.BoatId == BoatId).Include(b => b.Attrication)
                .ThenInclude(b => b.AttractionLocalized.Where(l => l.LanguageId == languageId));

            foreach (var item in BoatDestinationts)
            {
                BoatDestinationList.Add(item.Attrication.AttractionLocalized.Select(a => a.Title).FirstOrDefault());
            }
            return BoatDestinationList;
        }
    }
}
