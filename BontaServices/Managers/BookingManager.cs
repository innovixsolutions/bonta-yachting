﻿using BontaServices.Common;
using BontaServices.DTO;
using BontaServices.Models;
using HashidsNet;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BontaServices.Managers
{
    public class BookingManager
    {
        private readonly IHashids Hashids;
        private readonly BontaContext context;
        Random random = new Random();

        public BookingManager(IHashids Hashids, BontaContext Context)
        {
            this.Hashids = Hashids;
            context = Context;
        }
        private int GenereteRandomNumber()
        {
            return random.Next(1, 1000000);

        }
        private string GenereteBookingKey()
        {
            return Hashids.Encode(GenereteRandomNumber());
        }
        private Result<Customer> SaveOrUpdateCustomer(BoatBookingDTO boatBooking)
        {
            Customer Customer = new Customer
            {
                FirstName = boatBooking.FirstName,
                LastName = boatBooking.LastName,
                Email = boatBooking.Email,
                Phone = boatBooking.Phone,
            };

            if (!context.Customer.Any(f => f.Email == Customer.Email))
            {
                context.Entry(Customer).State = EntityState.Added;
                context.SaveChanges();
                Customer CustomerDetails = context.Customer.FirstOrDefault(x => x.Email == Customer.Email);
                boatBooking.CustomerId = CustomerDetails.Id;
                return Result.Ok(Customer);
            }

            Customer UpdatedCustomer = context.Customer.FirstOrDefault(x => x.Email == Customer.Email);
            UpdatedCustomer.FirstName = Customer.FirstName;
            UpdatedCustomer.LastName = Customer.LastName;
            UpdatedCustomer.Phone = Customer.Phone;
            boatBooking.CustomerId = UpdatedCustomer.Id;

            context.Entry(UpdatedCustomer).State = EntityState.Modified;
            return UpdatedCustomer is null ? Result.Fail<Customer>("No Customer Found") : Result.Ok(UpdatedCustomer);
        }

        public Result<BoatBlockedDate> AddBoatBlockedDate(DateTime CheckIn,int BoatId)
        {
            BoatBlockedDate blockedDate = new BoatBlockedDate()
            {
                BlockDate =CheckIn,
                BoatId = BoatId,
                BlockReason = 0//TODO add Enum for BlockReason
            };
            try
            {
                context.Entry(blockedDate).State = EntityState.Added;
                context.SaveChanges();
                return Result.Ok(blockedDate);
            }
            catch (Exception ex)
            {
                return Result.Fail<BoatBlockedDate>(ex.Message);
            }

        }
        public Result<BoatBooking> SaveBoatBooking(BoatBookingDTO boatBooking)
        {
            var CustomerSaveResult = SaveOrUpdateCustomer(boatBooking);
            if (CustomerSaveResult.IsSuccess)
            {
                
                BoatBooking booking = new BoatBooking()
                {
                    Adults = boatBooking.Adults,
                    Children = boatBooking.Children,
                    CheckIn = boatBooking.CheckIn,
                    CustomerId = boatBooking.CustomerId,
                    BookingKey=boatBooking.BookingKey = GenereteBookingKey(),
                    BoatId = boatBooking.BoatId,
                    Destination=boatBooking.Destination,
                    PaymentReference = " ",
                    PaymentStatus = 1,
                    SpecialRequest =boatBooking.SpecialRequst,
                    TotalPrice = boatBooking.Price = ((int)(boatBooking.Price))


                };
                try
                {
                    context.Entry(booking).State = EntityState.Added;
                    context.SaveChanges();
                    //AddBoatBlockedDate(boatBooking);
                    return Result.Ok(booking);
                }
                catch (Exception ex)
                {
                    return Result.Fail<BoatBooking>(ex.Message);
                }
            }
                return Result.Fail<BoatBooking>("Boat Booking Failed");



        }

        public BoatBooking GetBoatBooking(string BookingKey)
        {
            return context.BoatBooking.Where(a => a.BookingKey == BookingKey).Include(a=>a.Customer).Include(a=>a.Boat).FirstOrDefault();
        }
        public Result<BoatBooking> UpdateBoatBooking(BoatBooking boatBooking)
        {
            try
            {
                context.Entry(boatBooking).State = EntityState.Modified;

                context.SaveChanges();
                return Result.Ok(boatBooking);
            }catch (Exception ex)
            {
                return Result.Fail<BoatBooking>(ex.Message);
            }
         
        }
    }
}
