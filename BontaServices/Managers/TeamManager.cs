﻿using BontaServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.Managers
{
    public class TeamManager
    {
        private readonly BontaContext bontaContext;

        public TeamManager(BontaContext bontaContext)
        {
            this.bontaContext = bontaContext;
        }

        public IQueryable<TeamMember> GetTeamMembers()
        {
           return bontaContext.TeamMember.Where(t => t.Active.Value);
        }
    }
}
