﻿using BontaServices.Managers;
using BontaServices.Models;
using HashidsNet;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
namespace BontaServices
{
    public static class BontaServicesProviderResgistration
    {
        public static int LanguageId
        {
            get
            {
                return CultureInfo.CurrentCulture.LCID;
            }
        }


        public static IServiceCollection AddBontaServices(this IServiceCollection services)
        {




            services
                .AddSingleton<IHashids>(_ => new Hashids("BookingKey", 11))
                .AddScoped(s => new BoatManager(s.GetService<BontaContext>(), LanguageId))
                .AddScoped(s => new AttractionManager(s.GetService<BontaContext>(), LanguageId));
            services.AddScoped<BookingManager>();
            services.AddScoped<TeamManager>();
            services.AddScoped<AgentCodeManager>();

            return services;
        }
    }
}
