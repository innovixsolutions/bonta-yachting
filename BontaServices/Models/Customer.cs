﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace BontaServices.Models
{
    public partial class Customer
    {
        public Customer()
        {
            BoatBooking = new HashSet<BoatBooking>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<BoatBooking> BoatBooking { get; set; }
    }
}