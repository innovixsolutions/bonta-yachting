﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace BontaServices.Models
{
    public partial class BoatPhotos
    {
        public int Id { get; set; }
        public int BoatId { get; set; }
        public string Photo { get; set; }

        public virtual Boat Boat { get; set; }
    }
}