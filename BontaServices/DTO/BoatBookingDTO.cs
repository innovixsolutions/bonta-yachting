﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.DTO
{
    public class BoatBookingDTO
    { 
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public int PaymentStatus { get; set; }
        public string BookingKey { get; set; }
        public string PaymentReference { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime CheckIn { get; set; }
        public int BoatId { get; set; }
        public int CustomerId { get; set; }
        public decimal Price { get; set; }
        public string BoatName{ get; set; }
        public string Destination{ get; set; }

        public string SpecialRequst { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public string BusinessName { get; set; }
        public string WebsiteLink { get; set; }
        public string WebsiteText { get; set; }
        public string LogoFullPath { get; set; }
        public string FacebookLink { get; set; }
        public string TwitterLink { get; set; }
        public string HostName { get; set; }

    }
}
