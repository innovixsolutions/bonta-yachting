﻿using BontaServices.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.DTO
{
    public class BoatDTO
    {
        public int Id { get; set; } 
        public string UrlName { get; set; }
        public string MainPhoto { get; set; }
        public string BannerPhoto { get; set; }
        public int Passengers { get; set; }
        public double Length { get; set; }
        public double Draft { get; set; }
        public double MaxSpeed { get; set; }
        public string MainPhotoPath
        {
            get
            {
                return "/photos/boat/" + MainPhoto;
            }
        }
     
        public string BannerPhotoPath
        {
            get
            {
                return "/photos/boat/" + BannerPhoto;
            }
        }
        public string DetailsUrl
        {
            get
            {
                return "/fleet-details/" + UrlName;
            }
        }
     
        public string LocalizedName { get; set; }
     
        public string Description { get; set; }
       
        public string Exclude { get; set; }
        
        public string Include { get; set; }
        public string Extras { get; set; }
      
        public string HullMaterial { get; set; }
      
        public string Make { get; set; }
      
        public string MetaDescription { get; set; }
     
        public string Type { get; set; }

        public string MetaTitle { get; set; }

        public string WhatToBring { get; set; }
        public decimal? Price { get; set; }
        public string Notes { get; set; }
        public  List<BoatPhotos> BoatPhotos { get; set; }
        public List<string> Destinations { get; set; }
     
    }
}
