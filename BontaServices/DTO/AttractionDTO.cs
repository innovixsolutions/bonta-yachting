﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BontaServices.DTO
{
    public class AttractionDTO
    {
        public string Photo { get; set; }
        public string PhotoPath
        {
            get
            {
                return "/photos/attraction/" + Photo;
            }
        }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Highlights { get; set; }
    }
}
