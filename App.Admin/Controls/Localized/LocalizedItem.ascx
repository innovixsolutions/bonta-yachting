﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocalizedItem.ascx.cs"
    Inherits="DynamicData.Admin.Controls.Localized.LocalizedItem" %>

<%@ Register Src="~/Controls/Localized/LocalizedMetaTag.ascx" TagPrefix="uc1" TagName="LocalizedMetaTag" %>
<%@ Register Src="~/Controls/Localized/LocalizedNews.ascx" TagPrefix="uc1" TagName="LocalizedNews" %>


<%@ Register Src="~/Controls/Localized/LocalizedAttraction.ascx" TagPrefix="uc1" TagName="LocalizedAttraction" %>
<%@ Register Src="~/Controls/Localized/LocalizedBoat.ascx" TagPrefix="uc1" TagName="LocalizedBoat" %>
<%@ Register Src="~/Controls/Localized/LocalizedHomeBanner.ascx" TagPrefix="uc1" TagName="LocalizedHomeBanner" %>









<uc1:LocalizedBoat runat="server" ID="LocalizedBoat" Visible="False" />


<uc1:LocalizedMetaTag runat="server" ID="LocalizedMetaTag" Visible="False" />
<uc1:LocalizedNews runat="server" ID="LocalizedNews" Visible="False" />
<uc1:LocalizedHomeBanner runat="server" ID="LocalizedHomeBanner" Visible="False" />

<uc1:LocalizedAttraction runat="server" ID="LocalizedAttraction"  Visible="False" />

