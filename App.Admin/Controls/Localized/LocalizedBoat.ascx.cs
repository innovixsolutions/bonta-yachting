﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DynamicData.Admin.Infrastructure;
using DynamicData.Admin.Model;

namespace DynamicData.Admin.Controls.Localized
{
    public partial class LocalizedBoatItem : LocalizedControl
    {


        public override void BindData(int itemId, int languageId, string languageFriendlyName)
        {
            var db = new AdminEntities();
            var localizedItems = from l in db.BoatLocalizeds
                                 where l.BoatId == itemId && l.LanguageId == languageId
                                 select l;

            //Fields value
            if (localizedItems.Count() == 1) //Should be 1 for (details and edit), and 0 for (list and insert).
            {
                BoatLocalized[] localizedItemsArray = localizedItems.ToArray();
                lfName.FieldValue = localizedItemsArray[0].Name;
                lfDescription.FieldValue = localizedItemsArray[0].Description;
                lfMetaDescription.FieldValue = localizedItemsArray[0].MetaDescription;
                lfMetaTitle.FieldValue = localizedItemsArray[0].MetaTitle;
                lfMake.FieldValue = localizedItemsArray[0].Make;
                lfType.FieldValue = localizedItemsArray[0].Type;
                lfHullMaterial.FieldValue = localizedItemsArray[0].HullMaterial;
                lfInclude.FieldValue = localizedItemsArray[0].Include;
                lfExclude.FieldValue = localizedItemsArray[0].Exclude;
                lfNotes.FieldValue = localizedItemsArray[0].Notes;
                lfWhatToBring.FieldValue = localizedItemsArray[0].WhatToBring;
                lfExtras.FieldValue = localizedItemsArray[0].Extras;
            }

            //Fields language
            lfName.FieldLanguage =
                lfMake.FieldLanguage =
                 lfMetaTitle.FieldLanguage= lfDescription.FieldLanguage = lfMetaDescription.FieldLanguage= lfType.FieldLanguage=
                 lfHullMaterial.FieldLanguage=
                 lfInclude.FieldLanguage=
                 lfExclude.FieldLanguage=
                 lfNotes.FieldLanguage=
                 lfWhatToBring.FieldLanguage=
                 lfExtras.FieldLanguage

                 = languageFriendlyName;
        }

        public override void UpdateData(int itemId, int languageId)
        {
            var db = new AdminEntities();

            try
            {
                BoatLocalized item = db.BoatLocalizeds.Single(l => l.BoatId == itemId && l.LanguageId == languageId);
                item.Name = lfName.FieldValue;
                item.Description = lfDescription.FieldValue;
                item.MetaDescription = lfMetaDescription.FieldValue;
                item.MetaTitle = lfMetaTitle.FieldValue;
                item.Make = lfMake.FieldValue;
                item.Type = lfType.FieldValue;
                item.HullMaterial = lfHullMaterial.FieldValue;
                item.Include = lfInclude.FieldValue;
                item.Exclude = lfExclude.FieldValue;
                item.Notes = lfNotes.FieldValue;
                item.WhatToBring = lfWhatToBring.FieldValue;
                item.Extras = lfExtras.FieldValue;
                db.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == "Sequence contains no elements") // Should appear only if new languages just added.
                    InsertData(itemId, languageId);
                else throw ex;
            }
        }
        public override void InsertData(int itemId, int languageId)
        {
            var db = new AdminEntities();

            var item = new BoatLocalized
            {
                BoatId = itemId,
                LanguageId = languageId,
                Name = lfName.FieldValue,
                Description=lfDescription.FieldValue,
                MetaTitle = lfMetaTitle.FieldValue,
                MetaDescription = lfMetaDescription.FieldValue,
                Make=lfMake.FieldValue,
               Type=lfType.FieldValue,
               HullMaterial= lfHullMaterial.FieldValue,
               Include= lfInclude.FieldValue,
               Exclude= lfExclude.FieldValue,
               Notes= lfNotes.FieldValue,
               WhatToBring= lfWhatToBring.FieldValue,
                Extras = lfExtras.FieldValue,
        };
            db.BoatLocalizeds.Add(item);
            db.SaveChanges();
        }

    }
}