﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocalizedBoat.ascx.cs" Inherits="DynamicData.Admin.Controls.Localized.LocalizedBoatItem" %>
<%@ Register Src="LocalizedField.ascx" TagName="LocalizedField" TagPrefix="uc1" %>


<uc1:LocalizedField ID="lfName" FieldName="Name" 
    runat="server" RequiredLanguage="English" />


<uc1:LocalizedField ID="lfDescription" FieldName="Description" 
    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfMake" FieldName="Make" 
    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfType" FieldName="Type" 
    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfHullMaterial" FieldName="Hull Material" 
    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfInclude" FieldName="Include" 
    runat="server" RequiredLanguage="English"  FieldHint ="Please Insert one Per line"/>


<uc1:LocalizedField ID="lfExclude" FieldName="Exclude" 
    runat="server" RequiredLanguage="English" FieldHint ="Please Insert one Per line" />

<uc1:LocalizedField ID="lfExtras" FieldName="Extras" 
    runat="server" RequiredLanguage="English" FieldHint ="Please Insert one Per line" />


<uc1:LocalizedField ID="lfNotes" FieldName="Notes" 
    runat="server" RequiredLanguage="English" FieldHint ="Please Insert one Per line" />


<uc1:LocalizedField ID="lfWhatToBring" FieldName="WhatToBring" 
    runat="server" RequiredLanguage="English" FieldHint ="Please Insert one Per line" />

<uc1:LocalizedField ID="lfMetaTitle" FieldName="Meta Title" 
    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfMetaDescription" FieldName="Meta Description" 
    runat="server" RequiredLanguage="English" />


