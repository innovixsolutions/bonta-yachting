﻿using DynamicData.Admin.Infrastructure;
using DynamicData.Admin.Model;
using System;
using System.Linq;

namespace DynamicData.Admin.Controls.Localized
{
    public partial class AttractionItem : LocalizedControl
    {
        public override void BindData(int itemId, int languageId, string languageFriendlyName)
        {
            var db = new AdminEntities();
            var localizedItems = from l in db.AttractionLocalizeds
                                 where l.AttractionId == itemId && l.LanguageId == languageId
                                 select l;

            //Fields value
            if (localizedItems.Count() == 1) //Should be 1 for (details and edit), and 0 for (list and insert).
            {
                AttractionLocalized[] localizedItemsArray = localizedItems.ToArray();
                lfTitle.FieldValue = localizedItemsArray[0].Title;
                lfSubTitle.FieldValue = localizedItemsArray[0].SubTitle;
                lfHighlights.FieldValue = localizedItemsArray[0].Highlights;
               
            }

            //Fields language
            lfTitle.FieldLanguage =
            lfSubTitle.FieldLanguage =
            lfHighlights.FieldLanguage = languageFriendlyName;
        }

        public override void UpdateData(int itemId, int languageId)
        {
            var db = new AdminEntities();

            try
            {
                AttractionLocalized item =
                    db.AttractionLocalizeds.Single(l => l.AttractionId == itemId && l.LanguageId == languageId);

                item.Title = lfTitle.FieldValue;
                item.SubTitle = lfSubTitle.FieldValue;
                item.Highlights = lfHighlights.FieldValue;
               
                db.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == "Sequence contains no elements") // Should appear only if new languages just added.
                    InsertData(itemId, languageId);
                else throw ex;
            }
        }

        public override void InsertData(int itemId, int languageId)
        {
            var db = new AdminEntities();

            var item = new AttractionLocalized
            {
                AttractionId = itemId,
                LanguageId = languageId,
                Title= lfTitle.FieldValue,
                SubTitle = lfSubTitle.FieldValue,
               Highlights= lfHighlights.FieldValue
            };
            db.AttractionLocalizeds.Add(item);
            db.SaveChanges();
        }

        
    }
}