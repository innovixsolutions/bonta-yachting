﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocalizedAttraction.ascx.cs" Inherits="DynamicData.Admin.Controls.Localized.AttractionItem" %>
<%@ Register Src="LocalizedField.ascx" TagName="LocalizedField" TagPrefix="uc1" %>


<uc1:LocalizedField ID="lfTitle" FieldName="Title"  FieldIsRequired="true" 
                    runat="server" RequiredLanguage="English" />

<uc1:LocalizedField ID="lfSubTitle" FieldName="SubTitle" FieldIsRequired="true" 
                        runat="server"  />

<uc1:LocalizedField ID="lfHighlights" FieldName="Highlights" FieldIsRequired="true" 
                    runat="server" RequiredLanguage="English" FieldHint ="Please Insert Highlight one Per line" />
