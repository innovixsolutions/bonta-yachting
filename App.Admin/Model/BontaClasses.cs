﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.ComponentModel;
//using Microsoft.Data.OData.Query.SemanticAst;
using NotAClue.ComponentModel.DataAnnotations;

namespace DynamicData.Admin.Model
{


    #region Boats
    [MetadataType(typeof(BoatMetadata))]

    public partial class Boat
    {
    }

    public class BoatMetadata
    {
        [PageName]
        public object UrlName { get; set; }
        [UIHint("Photo")]
        [Photo("/photos/boat/originals/", "/photos/boat/")]
        [PhotoThumbnail(1, 0, "210,128,false,sm,.jpg", "580,385,false,lg,.jpg")]
        [Hint(Hint = "Please upload a 635 X 385 photo")]

        public object MainPhoto { get; set; }
        [UIHint("Photo")]
        [Photo("/photos/boat/originals/", "/photos/boat/")]
        [PhotoThumbnail(1, 0, "300,65,false,sm,.jpg", "1920,340,false,lg,.jpg")]
        [Hint(Hint = "Please upload a 1920 X 340 photo")]

        public object BannerPhoto { get; set; }

        
        [ScaffoldColumn(false)]
        public object BoatPhotos { get; set; }

        [ScaffoldColumn(false)]
        public object BoatLocalizeds{ get; set; }
        [ScaffoldColumn(false)]
        public object BoatRates { get; set; }
        [ScaffoldColumn(false)]
        public object BoatBlockedDates { get; set; }
     
        

    }
    #endregion
    #region BoatBooking
    [MetadataType(typeof(BoatBookingMetadata))]
    [DisableActions(DisableEditing = false,DisableDelete =false)]
    public partial class BoatBooking
    {
    }

    public class BoatBookingMetadata
    {
       [ScaffoldColumn(false)]
       public Object PaymentReference { get; set; }



    }
    #endregion
    #region Attractions
    [MetadataType(typeof(AttractionMetadata))]

    public partial class Attraction
    {
    }

    public class AttractionMetadata
    {
       
        [UIHint("Photo")]
        [Photo("/photos/attraction/originals/", "/photos/attraction/")]
        [PhotoThumbnail(1, 0, "210,128,false,sm,.jpg", "580,385,false,lg,.jpg")]
        [Hint(Hint = "Please upload a 635 X 385 photo")]

        public object Photo { get; set; }
       [ScaffoldColumn(false)]
       public object AttractionLocalizeds { get; set; }

    }
    #endregion
    #region TeamMember
    [MetadataType(typeof(TeamMemberMetadata))]
    public partial class TeamMember
    {
    }

    public class TeamMemberMetadata
    {

        [UIHint("Photo")]
        [Photo("/photos/team/originals/", "/photos/team/")]
        [PhotoThumbnail(1, 0, "120,100,false,sm,.jpg", "280,280,false,lg,.jpg")]
        [Hint(Hint = "Please upload a 300 X 280 photo")]

        public object Photo { get; set; }
       
        [ScaffoldColumn(false)]
        public object Email { get; set; }
       
       

    }
    #endregion
    

}