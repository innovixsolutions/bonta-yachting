//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DynamicData.Admin.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class BoatLocalized
    {
        public int BoatId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Make { get; set; }
        public string Type { get; set; }
        public string HullMaterial { get; set; }
        public string Include { get; set; }
        public string Exclude { get; set; }
        public string Extras { get; set; }
        public string Notes { get; set; }
        public string WhatToBring { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
    
        public virtual Language Language { get; set; }
        public virtual Boat Boat { get; set; }
    }
}
